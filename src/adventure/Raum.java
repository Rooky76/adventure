package adventure;

public class Raum extends Game
{
	public String Beschreibung;
	




{	Raum Computerraum;
	Raum Flur;
	Raum Lehrerzimmer;
	Raum Abstellkammer;
	Raum Toilette;
	Raum Keller;

	public Raum (int);
	{
		super();
		
		Computerraum = new Raum();
		Flur = new Raum();
		Lehrerzimmer = new Raum();
		Abstellkammer = new Raum();
		Toilette = new Raum();
		Keller = new Raum();
		
		
		Flur.Beschreibung = "Sie stehen im Flur und k�nnen drei T�ren erkennen";
		Computerraum.Beschreibung = "Sie sind im Computerraum. Es ist dunkel und sie k�nnen nicht viel erkennen";
		Lehrerzimmer.Beschreibung = "Sie sind im Lehrerzimmer. Der Schreibtisch ist aufgebrochen";
		Abstellkammer.Beschreibung = " Hier stehen Putzmittel und Besen rum";
		Toilette.Beschreibung = "Der Geruch deutet schon an wo man sich hier befindet. Vielleicht sollte man den Raum doch noch weiter untersuchen";
		Keller.Beschreibung = " Das ist der Keller. Das Licht geht nicht an. Vielleicht sollten sie sich erst eine Lampe besorgen";
		
	}
	
}





@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
			+ ((Beschreibung == null) ? 0 : Beschreibung.hashCode());
	return result;
}





@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Raum other = (Raum) obj;
	if (Beschreibung == null) {
		if (other.Beschreibung != null)
			return false;
	} else if (!Beschreibung.equals(other.Beschreibung))
		return false;
	return true;
}





public String getBeschreibung() {
	return Beschreibung;
}





public void setBeschreibung(String beschreibung) {
	Beschreibung = beschreibung;
}
	
}
